#include <bits/stdc++.h>

#define elif else if
#define pb push_back
#define fir first
#define sec second
#define len(a) (int)(a.size())
#define fast ios_base::sync_with_stdio(0); cin.tie(0); cout.tie(0);
#define all(num) num.begin(),num.end()
#define files(num) freopen(num".in","r",stdin); freopen(num".out","w",stdout)

using namespace std;

typedef pair<int, int> pii;
typedef long long ll;

#define int ll

const long long DENSITY_AURUM = 19300; //19300 kg/(m^3)
const long long SHC_AURUM = 130; // 130 J/kg*C
const long long MIN_WINDOW_HEIGHT = 3;
const long long MAX_WINDOW_HEIGHT = 10;
const long long  MIN_WINDOW_WIDTH = 3;
const long long  MAX_WINDOW_WIDTH = 10;
const long long MAX_LENGTH = 10;
const long long MAX_HEIGHT = 10;
const long long MAX_WIDTH = 10;
const long long MIN_ENERGY_FOR_RETURN = 1000000;
const long long MAX_ENERGY_FOR_RETURN = 1000000000;
const long long MIN_SHIP_VOLUME = 100;
const long long MAX_SHIP_VOLUME = 700;
const long long MIN_SHIP_ENERGY = 1000000000;
const long long MAX_SHIP_ENERGY = 10000000000;
const long long MAX_NUMBER_OF_INGOTS = 100;

ll window_height, window_width;
ll energy_for_return, ship_volume, ship_energy;

struct ingot
{
    ll ingot_length;
    ll ingot_height;
    ll ingot_width;
    ll ingot_weight;
    ll ingot_volume;
};

ingot random_ingot()
{
    ingot gen;
    gen.ingot_length = rand()%(MAX_LENGTH+1)+1;
    gen.ingot_height = rand()%(MAX_HEIGHT+1)+1;
    gen.ingot_width = rand()%(MAX_WIDTH+1)+1;
    gen.ingot_volume = gen.ingot_length*gen.ingot_height*gen.ingot_width;
    gen.ingot_weight = gen.ingot_volume*DENSITY_AURUM;
    return gen;
}

void rotate_ingot(ingot &kek)
{
    cout << "Rotate" << endl;
    swap(kek.ingot_length, kek.ingot_width);
}
void roll_ingot(ingot &kek)
{
    cout << "Roll" << endl;
    swap(kek.ingot_height, kek.ingot_width);
}

int get_position(ingot kek)
{
    if (kek.ingot_height<=window_height && kek.ingot_width <=window_width)
        return 0;
    if (kek.ingot_width<=window_height && kek.ingot_height <=window_width)
        return 1;
    if (kek.ingot_height<=window_height && kek.ingot_length <=window_width)
        return 2;
    if (kek.ingot_width<=window_height && kek.ingot_length <=window_width)
        return 3;
    if (kek.ingot_length<=window_height && kek.ingot_height <=window_width)
        return 4;
    if (kek.ingot_length<=window_height && kek.ingot_width <=window_width)
        return 5;
    return -1;
}

void pose_ingot(ingot &kek, int pos)
{
    if (pos == 0) return;
    if (pos == 1)
    {
        roll_ingot(kek);
        return;
    }
    if (pos == 2)
    {
        rotate_ingot(kek);
        return;
    }
    if (pos == 3)
    {
        rotate_ingot(kek);
        roll_ingot(kek);
        return;
    }
    if (pos == 4)
    {
        roll_ingot(kek);
        rotate_ingot(kek);
        return;
    }
    if (pos == 5)
    {
        rotate_ingot(kek);
        roll_ingot(kek);
        rotate_ingot(kek);
        return;
    }
}

void cut()
{
    cout << "Cut!" << endl;
}

void try_to_accept(ingot kek, int &cur_energy, int &cur_volume)
{
    int heat = SHC_AURUM*kek.ingot_weight;
    int volume = kek.ingot_volume;
   // cerr << heat << ' ' << volume << " :: try to accept" << endl;
    if (ship_energy-heat-cur_energy>=energy_for_return && volume+cur_volume<=ship_volume)
    {
        cur_energy+=heat;
        cur_volume+=volume;
        cout << "Accept 1.0" << endl;
        if (ship_energy-cur_energy == energy_for_return || cur_volume == ship_volume)
            cut();
    }
    else
    {
        int max_heat = ship_energy-cur_energy-energy_for_return;
        int max_volume = ship_volume-cur_volume;
        int s = kek.ingot_height*kek.ingot_width;
        double accept_for_heat = 1.0*max_heat/(DENSITY_AURUM*SHC_AURUM*s);
        double accept_for_volume = 1.0*max_volume/s;
        cerr << "could accept:: " << accept_for_heat << ' ' << accept_for_volume << endl;
        double accept = min(accept_for_heat, accept_for_volume);
        cur_energy+=max_heat;
        cout << "Accept " << accept/kek.ingot_length << endl;
        cut();
    }
}


signed main()
{
    cout << "Landing..." << endl;
    srand(time(NULL));
    window_height = rand()%(MAX_WINDOW_HEIGHT-MIN_WINDOW_HEIGHT+1) + MIN_WINDOW_HEIGHT;
    window_width = rand()%(MAX_WINDOW_WIDTH-MIN_WINDOW_WIDTH+1) + MIN_WINDOW_WIDTH;
    cout << "FURNACE::" << endl;
    cout << "Window height: " << window_height << "  ||  Window width: " << window_width << endl;
    cout << "SHIP:: " << endl;
    ship_energy = rand()%(MAX_SHIP_ENERGY-MIN_SHIP_ENERGY+1)+MIN_SHIP_ENERGY;
    ship_volume = rand()%(MAX_SHIP_VOLUME-MIN_SHIP_VOLUME+1)+MIN_SHIP_VOLUME;
    energy_for_return = rand()%(MAX_ENERGY_FOR_RETURN-MIN_ENERGY_FOR_RETURN+1)+MIN_ENERGY_FOR_RETURN;
    cout << "Ship energy: " << ship_energy << endl;
    cout << "Ship volume: " << ship_volume << endl;
    cout << "Energy for return: " << energy_for_return << endl;
    int current_volume = 0;
    int current_energy = 0;
    int counter = 0;
    while(current_volume<ship_volume && ship_energy-current_energy>energy_for_return)
    {
        //cerr << current_energy << ' ' << current_volume << endl;
        if (counter == MAX_NUMBER_OF_INGOTS)
        {
            cout << "End of ingots((" << endl;
            cut();
            break;
        }
        counter++;
        ingot cur = random_ingot();
        cout << "A NEW INGOT WAS GENERATED\n";
        cout << "length: " << cur.ingot_length << "  |  height: " << cur.ingot_height << "  |  width: " << cur.ingot_width << endl;
        cout << "volume: " << cur.ingot_volume << "  ||  weight: " << cur.ingot_weight << endl;
        int pos = get_position(cur);
        if (pos == -1)
        {
            cout << "Dropped" << endl;
        }
        else
        {
            pose_ingot(cur,pos);
            try_to_accept(cur, current_energy, current_volume);
        }
         //cerr << current_energy << ' ' << current_volume << endl;

    }
    cout << "Fly" << endl;

    return 0;
}
